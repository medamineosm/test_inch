package com.inch.evaluation.Service;

import com.inch.evaluation.Metiers.Days;
import org.joda.time.DateTime;

/**
 * Created by OUASMINE Mohammed Amine on 01/09/2016.
 */
public class Utils {
    public static Days getDayByNumber(int number){
        switch (number){
            case 1:
                return Days.LUNDI;
            case 2:
                return Days.MARDI;
            case 3:
                return Days.MERCREDI;
            case 4:
                return Days.JEUDI;
            case 5:
                return Days.VENDREDI;
            case 6:
                return Days.SAMEDI;
            case 7:
                return Days.DIMANCHE;
            default:
                return null;
        }
    }

    public static String displayDate(DateTime date){
        return  date.getYear()+"/"+date.getMonthOfYear()+"/"+date.getDayOfMonth()
                +" "+ Utils.getDayByNumber(date.getDayOfWeek())
                +" | "+ date.getHourOfDay()+":"+date.getMinuteOfHour();
    }
}
