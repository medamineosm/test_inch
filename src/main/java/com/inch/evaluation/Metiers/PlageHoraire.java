package com.inch.evaluation.Metiers;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by OUASMINE Mohammed Amine on 01/09/2016.
 */
public class PlageHoraire {

    private int startHour;
    private int endHour;


    public PlageHoraire(int startHour,int endHour){
        this.startHour = startHour;
        this.endHour = endHour;
    }

    public boolean isTopLimite(DateTime date){
        Date d = date.toDate();
        d.setHours(this.startHour);
        d.setMinutes(0);
        d.setSeconds(0);
        return !date.isBefore(new DateTime(d));
    }

    public boolean isBottomBorneLimite(DateTime date){
        Date d = date.toDate();
        d.setHours(this.endHour);
        d.setMinutes(0);
        d.setSeconds(0);
        return !date.isAfter(new DateTime(d));
    }

    @Override
    public String toString() {
        return "PlageHoraire{" +
                "startHour=" + startHour +
                ", endHour=" + endHour +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlageHoraire that = (PlageHoraire) o;

        if (startHour != that.startHour) return false;
        return endHour == that.endHour;

    }

    @Override
    public int hashCode() {
        int result = startHour;
        result = 31 * result + endHour;
        return result;
    }
}
