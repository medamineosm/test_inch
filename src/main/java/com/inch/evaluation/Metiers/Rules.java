package com.inch.evaluation.Metiers;

import com.inch.evaluation.Service.Utils;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by OUASMINE Mohammed Amine on 01/09/2016.
 */
public class Rules {
    private String entrepriseName;
    private Map<Days,Set<Rule>> rules;


    public Rules(String entrepriseName) {
        this.entrepriseName = entrepriseName;
        this.rules = new HashMap<>();
    }

    public void addRule(Rule rule){
        if(!this.rules.containsKey(rule.getDays())){
            Set<Rule> rules_tmp = new HashSet<>();
            rules_tmp.add(rule);
            this.rules.put(rule.getDays(), rules_tmp);
        }else{
            this.rules.get(rule.getDays()).add(rule);
        }
    }

    public boolean isAvailableShift(DateTime date){
        Set<Rule> list_Rules= this.rules.get(Utils.getDayByNumber(date.getDayOfWeek()));
        if(list_Rules != null){
            for(Rule rule : list_Rules){
                if(rule.isRespected(date))
                    return true;
            }
        }
        return false;
    }


    public void display(){
        System.out.println("entrepriseName='" + entrepriseName);
        for(Set<Rule> rule : this.rules.values()){
            System.out.println("\t"+rule.toString());
        }
    }
}
