package com.inch.evaluation.Metiers;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by OUASMINE Mohammed Amine on 01/09/2016.
 */
public class Test {

    public static void main(String[] args){

        String name = "Pixalione";

        Date d = new Date();
        d.setDate(31);
        d.setMonth(8);
        d.setHours(8);


        Rule r1 = new Rule(Days.LUNDI,new PlageHoraire(8,12));
//        Rule r11 = new Rule(Days.LUNDI,new PlageHoraire(13,17));
//        Rule r2 = new Rule(Days.MARDI,new PlageHoraire(9,12));
//        Rule r22 = new Rule(Days.MARDI,new PlageHoraire(13,17));
//        Rule r3 = new Rule(Days.MERCREDI,new PlageHoraire(9,12));
//        Rule r33 = new Rule(Days.MERCREDI,new PlageHoraire(13,17));

        Rules rules = new Rules(name);
        rules.addRule(r1);
//        rules.addRule(r2);
//        rules.addRule(r11);
//        rules.addRule(r22);
//        rules.addRule(r3);
//        rules.addRule(r33);

//        rules.display();

        Agenda agenda = new Agenda(name,1,rules, true);
//        System.out.println(agenda.getShift());
        DateTime start = new DateTime();
        start = start.plusDays(3);
        start = start.plusHours(12);
        DateTime end = start.plusMinutes(30);

        Event event = new Event(true, true,start, end);

        agenda.addEvent(event);

//        System.out.println(agenda.getLocked_Shifts());


        JsonFormat.display(Event.availabilities(agenda.getShift()));
    }
}
