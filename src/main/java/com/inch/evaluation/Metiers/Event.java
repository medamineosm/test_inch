package com.inch.evaluation.Metiers;

/**
 * Created by OUASMINE Mohammed Amine on 31/08/2016.
 */
import com.inch.evaluation.Service.Utils;
import org.joda.time.DateTime;

import java.util.*;

public class Event {
    private boolean opening;
    private boolean recurring;
    private DateTime startDate;
    private DateTime endDate;


    public Event(boolean opening, boolean recurring, DateTime startdate, DateTime endDate) {
        super();
        this.opening = opening;
        this.recurring = recurring;
        this.startDate = startdate;
        this.endDate = endDate;
    }

    public void setOpening(boolean opening) {
        this.opening = opening;
    }

    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public boolean isOpening() {
        return opening;
    }

    public boolean isBusy() {
        return !opening;
    }

    public boolean isRecurring() {
        return recurring;
    }

    @Override
    public String toString() {
        return "Event{" +
                "opening=" + opening +
                ", recurring=" + recurring +
                ", startDate=" + Utils.displayDate(startDate) +
                ", endDate=" + Utils.displayDate(endDate) +
                '}';
    }

    public static Collection<JsonFormat> availabilities(Collection<Shift> shifts)
    {
        Collection<JsonFormat> result = new HashSet<>();
        for(Shift shift : shifts){
            if(shift.isLocked()){
                result.add(new JsonFormat(shift.getDateString(),Arrays.asList((shift.displayHourMinute()))));
            }
        }
        return result;
    }



}
