package com.inch.evaluation.Metiers;

import java.util.*;

/**
 * Created by AmYné on 01/09/2016.
 */

public class JsonFormat {

    private String day;
    private List<String> window;

    public JsonFormat(String day, List<String> window) {
        this.day = day;
        this.window = window;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<String> getwindow() {
        return window;
    }

    public void setwindow(List<String> window) {
        this.window = window;
    }

    @Override
    public String toString() {
        return "JsonFormat{" +
                "day='" + day + '\'' +
                ", window=" + window +
                '}';
    }

    public static  Map<String,Set<String>> display(Collection<JsonFormat> jsonFormats){
        Map<String,Set<String>> result = new HashMap<>();
        for(JsonFormat jsonFormat : jsonFormats){
            if(!result.keySet().contains(jsonFormat.getDay())){
                result.put(jsonFormat.getDay(),new HashSet<>());
            }
            result.get(jsonFormat.getDay()).addAll(jsonFormat.getwindow());
        }
        test(result);
        return result;
    }

    private static void test( Map<String,Set<String>> r){
        for(String key : r.keySet()){
            System.out.println("Date: "+ key +", windows :"+r.get(key));
        }
    }
}
