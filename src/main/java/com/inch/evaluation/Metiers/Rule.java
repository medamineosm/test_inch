package com.inch.evaluation.Metiers;

import org.joda.time.DateTime;

/**
 * Created by OUASMINE Mohammed Amine on 01/09/2016.
 */
public class Rule {

    private Days days;
    private PlageHoraire plageHoraire;

    public Rule(PlageHoraire plageHoraire) {
        this.plageHoraire = plageHoraire;
    }

    public Rule(Days days, PlageHoraire plageHoraire) {
        this.days = days;
        this.plageHoraire = plageHoraire;
    }

    public Days getDays() {
        return days;
    }

    public PlageHoraire getPlageHoraire() {
        return plageHoraire;
    }

    public boolean isRespected(DateTime date){
       return this.plageHoraire.isBottomBorneLimite(date) && this.plageHoraire.isTopLimite(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        if (days != rule.days) return false;
        return plageHoraire != null ? plageHoraire.equals(rule.plageHoraire) : rule.plageHoraire == null;

    }

    @Override
    public int hashCode() {
        int result = days.hashCode();
        result = 31 * result + (plageHoraire != null ? plageHoraire.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "days=" + days +
                "," +
                "plageHoraire=" + plageHoraire +
                '}';
    }
}
