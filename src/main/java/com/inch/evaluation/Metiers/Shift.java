package com.inch.evaluation.Metiers;

import com.inch.evaluation.Service.Utils;
import org.joda.time.DateTime;

/**
 * Created by AmYné on 01/09/2016.
 */
public class Shift {

    private DateTime startDate;
    private DateTime endDate;
    private int step;
    private boolean locked;

    public Shift(DateTime startDate, int step, boolean locked) {
        this.startDate = startDate;
        this.step = step;
        this.endDate = this.startDate.plusMinutes(30);
        this.locked = false;
    }

    @Override
    public String toString() {
        return "Shift{" +
                "startDate=" + Utils.displayDate(startDate) +
                ", endDate=" + Utils.displayDate(endDate) +
                ", locked=" + locked +
                '}';
    }

    public void lock(){
        this.locked = true;
    }

    public boolean isIn(Event event){
        boolean result = Utils.getDayByNumber(event.getStartDate().getDayOfWeek()).equals(Utils.getDayByNumber(this.startDate.getDayOfWeek()));
        //System.out.println(Utils.getDayByNumber(event.getStartDate().getDayOfWeek())+" <> "+Utils.getDayByNumber(this.startDate.getDayOfWeek())+" = "+result);

        if(!Utils.getDayByNumber(event.getStartDate().getDayOfWeek()).equals(Utils.getDayByNumber(this.startDate.getDayOfWeek()))) return false;
        return this.startDate.isAfter(event.getStartDate()) ||
                this.endDate.isBefore(event.getEndDate()) ||
                this.startDate.isEqual(event.getStartDate()) ||
                this.endDate.isEqual(event.getEndDate());
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public int getStep() {
        return step;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getDateString(){
        return this.getStartDate().year().getAsString()+
                "-"+this.getStartDate().monthOfYear().getAsString()+
                "-"+this.getStartDate().dayOfMonth().getAsString();
    }

    public String[] displayHourMinute(){
        String[] result = new String[2];
        result[0]= this.getStartDate().getHourOfDay()+":"+this.getStartDate().getMinuteOfHour();
        result[1]= this.getEndDate().getHourOfDay()+":"+this.getEndDate().getMinuteOfHour();
        return  result;
    }


}
