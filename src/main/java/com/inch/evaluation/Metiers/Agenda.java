package com.inch.evaluation.Metiers;

import org.apache.tomcat.util.modeler.Util;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by OUASMINE Mohammed Amine on 01/09/2016.
 */
public class Agenda {
    private boolean flag = true;
    private String entrepriseName;
    private int year;
    private int limit;
    private DateTime date_Start;
    private DateTime date_End;
    private Set<Shift> shifts;
    private Set<Shift> locked_Shifts;
    private int step = 30;
    private Rules rules;

    public Agenda(){}

    public Agenda(String entrepriseName,int limit ,Rules rules,boolean flag) {
        this.limit = limit;
        this.entrepriseName = entrepriseName;
        this.date_Start = new DateTime();
        this.date_End = this.date_Start.plusMonths(this.limit);
        this.year = this.date_Start.getYear();
        this.rules = rules;
        this.flag = flag;
        this.locked_Shifts = new HashSet<>();
        this.initShift(this.date_Start);
    }

    @Override
    public String toString() {
        return "Agenda{" +
                "entrepriseName='" + entrepriseName + '\'' +
                ", year=" + year +
                ", limit=" + limit +
                ", date_Start=" + date_Start +
                ", date_End=" + date_End +
                ", Shift=" + shifts +
                '}';
    }

    public void initShift(DateTime date){

        if(flag){
            date = new DateTime(roundDate(date));
            flag = false;
        }

        if(this.rules.isAvailableShift(date)){
            this.getShift().add(new Shift(date,this.step, false));
        }
        if(date.isBefore(this.date_End)){
            this.initShift(date.plusMinutes(this.step));
        }
    }

    private Date roundDate(DateTime date){
        Date date_tmp = date.toDate();
        if(date.toDate().getMinutes() > 30){
            date_tmp.setMinutes(0);
            date_tmp.setHours(date_tmp.getHours()+1);
        }else if(date.toDate().getMinutes() < 30){
            date_tmp.setMinutes(0);
        }
        return date_tmp;
    }

    public void addEvent(Event event){
        System.out.println(event);
        if(event.isOpening()){
            if(event.isRecurring()){
                for (Shift shift : this.shifts){
                    if(shift.isIn(event)){
                        shift.setLocked(true);
                        this.locked_Shifts.add(shift);
                    }
                }
            }
        }
    }

    public Set<Shift> getLocked_Shifts() {
        return locked_Shifts;
    }

    public Set<Shift> getShift() {
        if(this.shifts == null){
            this.shifts = new HashSet<>();
        }
        return shifts;
    }
}
