package com.inch.evaluation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InchApplication {

	public static void main(String[] args) {
		SpringApplication.run(InchApplication.class, args);
	}
}
